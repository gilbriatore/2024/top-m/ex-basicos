﻿
/***************************************************************************
1. Tipos de variáveis var, int, short, long, decimal, double, bool e string;
2. Operadores de atribuição, aritméticos, relacionais, lógicos e unários;
3. Estruturas de decisão if/else, ternárias e switch;
4. Estruturas de repetição while, do/while, for, foreach;
5. Arrays.
***************************************************************************/

//Console.WriteLine("Olá mundo C#!");

//1. Tipos de variáveis var, int, short, long, decimal, double, bool e string;
int a = 10;
var b = 5;
//int c = "Algum texto";
var d = "Algum texto" + 10;
double e = 10.5;
bool aprovado = true;
string txt = "Algum texto";

double xyz;

//2. Operadores de atribuição, aritméticos, relacionais, lógicos e unários;

xyz = 100.6;
double zyx = 30.7;
double aaa = xyz + zyx;
Console.WriteLine("Valor " + aaa);

//3. Estruturas de decisão if/else, ternárias e switch;

bool temp = a == 5 ? true : false;

if (xyz > 100)
{
    Console.WriteLine("Valor maior do que 100");
}


// 4. Estruturas de repetição while, do/while, for, foreach;
// 5. Arrays.

int[] vetor1 = new int[] { 1, 5, 7, 8, 9 };
int[] vetor2 = new int[4];

int vlrVetor1 = vetor1[2];
int vlrVetor2 = vetor2[1];

Console.WriteLine("Valor vetor 1 " + vlrVetor1);
Console.WriteLine("Valor vetor 2 " + vlrVetor2);

for (int i = 0; i < 5; i++)
{
    int vlr = vetor1[i];
    Console.WriteLine("Valor no for " + vlr);
}

string[] dados = new string[] { "Ana", "Pedro", "Carlos", "Julia" };

int contador = 0;
do
{
    string nome = dados[contador];
    Console.WriteLine("Nome no do/while " + nome);
    contador++;
} while (contador < 4);

foreach (var nome in dados)
{
    Console.WriteLine("Nome no foreach " + nome);
}

// Entrada de dados pelo usuário

Console.WriteLine("Digite seu nome: ");
string nomeDigitado = Console.ReadLine();

Console.WriteLine("Digite sua idade: ");
string idadeDigitada = Console.ReadLine();

int idadeConvertida = Convert.ToInt32(idadeDigitada);

Console.WriteLine("Nome: " + nomeDigitado);
Console.WriteLine($"Idade: {idadeConvertida}" + "Alterado");